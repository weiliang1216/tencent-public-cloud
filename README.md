# 腾讯公有云HA README

## 准备工作
### 安装Python3
```
mkdir -p /tmp/python3
yum reinstall --downloadonly --downloaddir=/tmp/python3 python3 python3-libs python3-pip python3-setuptools
tar -cf python3.tar /tmp/python3
```

在yum中重新下载一个已经存在的rpm包，需要使用`--downloaddir`和`--downloonly`选项。下载后的包会被放在指定的目录中，可以使用`rpm -ivh --nodeps`命令来手动安装。

### 安装Python3 至 BIGIP
```
#略
```

## 安装腾讯Python SDK

```
mount -o remount,rw /usr/
pip3 install --upgrade tencentcloud-sdk-python
mount -o remount,ro /usr/
```
参考文档：https://github.com/TencentCloud/tencentcloud-sdk-python

## 国内用

```
pip3 install --upgrade tencentcloud-sdk-python -i https://mirrors.tencent.com/pypi/simple/
```

- 腾讯云Python SDK安装完毕
- 获取API Key 和 API Cred
- 每个VE配置2块及以上的弹性网卡（ENI）
    - 主网卡是mgmt
    - 辅助网卡 - 第一个IP不要配置业务，空置
    - Self IP使用辅助网卡的第二个IP，IP不够就配置第三网卡

```
import json
from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.vpc.v20170312 import vpc_client, models
try:
    # 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
    # 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
    # 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
    cred = credential.Credential("AKxxxxxxxxxxxxxxKFZ", "82kxxxxxxxxxxxxxxxxxxfmHao")
    # 实例化一个http选项，可选的，没有特殊需求可以跳过
    httpProfile = HttpProfile()
    httpProfile.endpoint = "vpc.tencentcloudapi.com"

    # 实例化一个client选项，可选的，没有特殊需求可以跳过
    clientProfile = ClientProfile()
    clientProfile.httpProfile = httpProfile
    # 实例化要请求产品的client对象,clientProfile是可选的
    client = vpc_client.VpcClient(cred, "ap-guangzhou", clientProfile)

    # 实例化一个请求对象,每个接口都会对应一个request对象
    req = models.MigratePrivateIpAddressRequest()
    params = {
        "SourceNetworkInterfaceId": "eni-26xjy0jn",
        "DestinationNetworkInterfaceId": "eni-3znrtcpv",
        "PrivateIpAddress": "172.16.32.51,"
    }
    req.from_json_string(json.dumps(params))

    # 返回的resp是一个MigratePrivateIpAddressResponse的实例，与请求对象对应
    resp = client.MigratePrivateIpAddress(req)
    # 输出json格式的字符串回包
    print(resp.to_json_string())

except TencentCloudSDKException as err:
    print(err)
```

AKxxxxxxxxxxxxxxKFZ和82kxxxxxxxxxxxxxxxxxxfmHao是SecretId 和 SecretKey，ap-guangzhou是VPC所在区域的名字，这里的 eni-26xjy0jn是另一台VE的网卡，eni-3znrtcpv是当前VE的网卡，172.16.32.51是要切换的IP地址，VE中的IP会自动切换，这里的切换是为了引导底层流量到当前激活的VE上。

## 部署自动激活脚本
将该脚本放置在/config下，命名onAct.py
```
echo “python3 /config/onAct.py” >> /config/failover/active

chcon -u system_u -r object_r -t f5config_failover_exec_t /config/onAct.py
```

## 后续 SELINUX问题
创建一个自定义的SELinux模块：

- 创建一个名为**`my_f5config.te`**的文本文件，并在其中输入以下内容：
### 腾讯模式：和SDK实现有关
```
module my_f5config 1.0;

require {
    type f5config_failover_t;
    type net_conf_t;
    class lnk_file { read };
}

allow f5config_failover_t net_conf_t:lnk_file { read };
```

编译和加载SELinux模块：

- 使用 **`checkmodule`** 命令将 **`.te`** 文件编译为 **`.mod`** 文件。运行以下命令：
```
checkmodule -M -m -o my_f5config.mod my_f5config.te
```

使用 semodule_package 命令将 .mod 文件打包为 .pp 文件。运行以下命令：
```
semodule_package -o my_f5config.pp -m my_f5config.mod
```

使用 semodule 命令加载新的 SELinux 模块。运行以下命令：
```
semodule -i my_f5config.pp
```

### Troubleshooting
查看Linux相关SELINUX的安全日志
```
grep avc /var/log/auditd/audit.log
```
或者
```
tail -f /var/log/auditd/audit.log |grep avc
```
查看SELINUX相关组
```
ls -Zl /config/failover/
```
